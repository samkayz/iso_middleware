# Generated by Django 3.1.1 on 2020-12-07 16:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('console', '0002_auto_20201204_0245'),
    ]

    operations = [
        migrations.CreateModel(
            name='ErrorMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.TextField(blank=True, null=True)),
                ('message', models.TextField(blank=True, null=True)),
            ],
            options={
                'db_table': 'error_massage',
            },
        ),
    ]
