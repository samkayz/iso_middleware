from __future__ import absolute_import, unicode_literals
from celery.task import periodic_task
from celery.schedules import crontab
from celery import shared_task
from .models import *
import string
import random
import datetime
import time
from datetime import timedelta
import os
from xml.etree import ElementTree as ET
import shutil
from lxml import etree
from function import *
import xmlschema
from pprint import pprint
from create_xml import *
from django.test.client import RequestFactory


my_funt = Main()
createXMLFile = CreateXMLS()

script_dir = os.path.dirname(os.path.abspath(__file__))
dest_dir = os.path.join('static/out')
newpath = os.path.join('static/in')
atsPath = os.path.join('static/ats')
processed = os.path.join('static/processed')
single_resp = os.path.join('static/single')

try:
    query_value = Config.objects.values('m_sec').get(name='mbroke')['m_sec']
    sec = int(query_value)
except:
    sec = 10


today = datetime.datetime.now()
now = today.strftime('%Y-%m-%d')


def Process_IN_file():
    for files in os.listdir(newpath):
        if os.path.exists(newpath):

            fullname = os.path.join(newpath, files)
            tree = ET.ElementTree(file=fullname)
            root = tree.getroot()
            np = root.tag[1:root.tag.index('}')]
            a = '{'
            b = '}'
            msgId = root[0][0].findtext(f'.//{a + np + b}MsgId')
            ordMsgId = root[0][1].findtext(f'.//{a + np + b}OrgnlMsgId')
            respMsg = root[0][1].findtext(f'.//{a + np + b}OrgnlMsgNmId') 
            batchStatus = root[0][1].findtext(f'.//{a + np + b}GrpSts')
            statusReason = root[0][1][3][1][0].text
            msg = my_funt.GetResponseMsg(statusReason)
            print(msg)
            batch_instance = ProcessBatch.objects.filter(batch_id=msgId)
            batch_instance.update(status=batchStatus, response=msg)
            txnInfo = root[0].findall(f'.//{a + np + b}TxInfAndSts')
            msgcode = my_funt.GetMsgCode(respMsg)
            for x in txnInfo:
                if msgcode == "CR":
                    StsId = x.findtext(f'.//{a + np + b}StsId')
                    OrgnlTxId = x.findtext(f'.//{a + np + b}OrgnlTxId')
                    TxSts = x.findtext(f'.//{a + np + b}TxSts')
                    Rsn = x.find(f'.//{a + np + b}StsRsnInf')[1][0].text
                    # print(Rsn)
                    smsg = my_funt.GetResponseMsg(Rsn)
                    instance = Payment.objects.filter(batctno=msgId, payee_id=OrgnlTxId)
                    instance.update(status=TxSts, response=smsg)
                else:
                    pass
        # shutil.move(f'{newpath}/{files}', processed)

        pass
__name__ == "__main__"


def Process_Single_Customer_Response():
    a = '{'
    b = '}'
    for files in os.listdir(single_resp):
        if os.path.exists(single_resp):
            filename = os.path.join(single_resp, files)
            tree = ET.ElementTree(file=filename)
            root = tree.getroot()
            root_np = root.tag[1:root.tag.index('}')]
            body = root.find(f'.//{a + root_np + b}Body')
            doc_np = body[1].tag[1:body[1].tag.index('}')]

            document = body[1]
            msgId = root.findtext(f'.//{a + root_np + b}MessageIdentifier')
            sndRef = root.findtext(f'.//{a + root_np + b}SenderReference')
            # print(sndRef)
            msgcode = my_funt.GetMsgCode(msgId)
            # print(msgId)
            # print(msgcode)
            if msgcode == "SRJ":
                reason = document[0].find(f'.//{a + doc_np + b}Rsn')[0].text
                status = document[0].findtext(f'.//{a + doc_np + b}GrpSts')
                # print(reason)
                # print(status)
                msg = my_funt.GetResponseMsg(reason)
                # print(msg)
                instance = SinglePayment.objects.filter(batch_id=sndRef)
                instance.update(status=status, response=msg)

                batch_instance = ProcessBatch.objects.filter(batch_id=sndRef)
                batch_instance.update(status=status)
            elif msgcode == "SRCB":
                status = "PROCESSED"
                reason = document[0].find(f'.//{a + doc_np + b}Rsn')[0].text
                # print(reason)
                msg = my_funt.GetResponseMsg(reason)
                # print(msg)

                instance = SinglePayment.objects.filter(batch_id=sndRef)
                instance.update(status=status, response=msg)

                batch_instance = ProcessBatch.objects.filter(batch_id=sndRef)
                batch_instance.update(status=status)
            
            elif msgcode == "SDBSA":
                status = "ACCEPTED"
                
                msg = "Transactions Accepted and Settlement Completed at ATS"
                instance = SinglePayment.objects.filter(batch_id=sndRef)
                instance.update(status=status, response=msg)

                batch_instance = ProcessBatch.objects.filter(batch_id=sndRef)
                batch_instance.update(status=status)

            else:
                pass
        shutil.move(f'{single_resp}/{files}', processed)
__name__ == "__main__"



def Process_ATS():
    for files in os.listdir(dest_dir):
        if os.path.exists(dest_dir):
            if not files.endswith('.xml'): continue

        fullname = os.path.join(dest_dir, files)
        tree = ET.parse(fullname)
        root = tree.getroot()
        code = root[0][0][0].text
        print(root[0][0][0].text)
        print(root[0][0][1].text)
        print(root[0][0][2].text)

        sch = Schema.objects.values('schemas').get(pac_name='pacs.008.001.07.xsd')['schemas']
        schema = xmlschema.XMLSchema(sch)
        print(schema.is_valid(f'{dest_dir}/{files}'))
        status = schema.is_valid(f'{dest_dir}/{files}')
        if status == False: continue
        ProcessBatch.objects.filter(batch_id=code).update(status="validated")
        File_Management.objects.filter(batch_id=code).update(status="validated")
        # Payment.objects.filter(batctno=code).update(status="validated")
        shutil.move(f'{dest_dir}/{files}', atsPath)

    pass
__name__ == "__main__"


def process_queue():
    today = datetime.datetime.now()
    d = datetime.datetime.now().replace(tzinfo=datetime.timezone.utc)
    no_ms = d.replace(microsecond=0)
    now = no_ms.isoformat()
    tnow = today.strftime('%Y-%m-%d')
    U = 7
    ref = ''.join(random.choices(string.digits, k=U))
    txn = str(ref)
    txnref = f'FT{txn}'
    all_queue_batch = ProcessBatch.objects.filter(status='queue', tnxtype='bulk')

    
    for batch in all_queue_batch:
        batch_id = batch.batch_id
        # print(batch_id)
        lists = []
        total_sum = []
        process_payment = Payment.objects.filter(batctno=batch_id, status='queue')
        for data in process_payment:
            # print(data)
            datas = {}
            datas['pay_id'] = data.payee_id
            datas['emp_name'] = data.employee_name
            datas['ben_bicode'] = data.bnkcode
            datas['ben_bank_name'] = data.bank_name
            datas['acct_no'] = data.acctno
            datas['pay_date'] = data.date
            datas['currency'] = data.currency
            datas['g_amt'] = data.gross_amount
            datas['tax_amt'] = data.tax_amount
            datas['amount'] = data.amount
            datas['desc'] = data.desc

            lists.append(datas)
            total_sum.append(float(data.amount))
        total = (sum(total_sum))
        total_record = (len(lists))
        # print(lists)
        # print(total)
        if my_funt.cut_Of_Time() == True and my_funt.GetWorkingDays() == True:
            createXMLFile.ReDOnewXML(batch_id, now, total_record, total, tnow, lists, txnref)
            pass
        else:
            print("Transaction is out of Cut of time")
            pass
__name__ == "__main__"


def Process_Single_Queue():
    U = 7
    ref = ''.join(random.choices(string.digits, k=U))
    txn = str(ref)
    txnref = f'FT{txn}'
    all_queue = ProcessBatch.objects.filter(status='queue', tnxtype='single')
    for batch in all_queue:
        batch_id = batch.batch_id
        process_pay = SinglePayment.objects.filter(batch_id=batch_id, status='queue')
        for data in process_pay:
            sname = data.sname
            saddress = data.saddress
            s_db_acc = data.s_db_acc
            rname = data.rname
            raddress = data.raddress
            r_cr_acc = data.r_cr_acc
            rbank = data.rbank
            amount = data.amount
            comment = data.comment
            if my_funt.cut_Of_Time() == True and my_funt.GetWorkingDays() == True:
                createXMLFile.SingleXML(amount, comment, sname, saddress, s_db_acc, rbank, rname, raddress, r_cr_acc, batch_id, txnref)
                Objects.Create_File_Management(batch_id, txnref)
                ProcessBatch.objects.filter(status='queue', tnxtype='single', batch_id=batch_id).update(status='created')
                SinglePayment.objects.filter(batch_id=batch_id, status='queue').update(status='created')
                pass
            else:
                print("Transaction is out of Cut of time")
                pass
            
__name__ == "__main__"



@periodic_task(run_every=timedelta(seconds=sec))
def Run_Task():
    if my_funt.Configuration(name='service') == '1':
        print("SERVICE ON")
        Process_ATS()
        Process_IN_file()
        process_queue()
        Process_Single_Queue()
        Process_Single_Customer_Response()
        pass

    else:
        print("SERVICE OFF")
