from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import AbstractBaseUser, UserManager, BaseUserManager
from django.http import response
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.sessions.models import Session

class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)
    
class User(AbstractBaseUser, PermissionsMixin):
    username = None
    email = models.EmailField(_('email address'), unique=True)
    fullname = models.TextField("fullname", null=True, blank=True)
    is_staff = models.BooleanField(verbose_name='is_staff', default=True)
    date_joined = models.DateField(verbose_name='date_joined', auto_now_add=True)
    is_active = models.BooleanField(verbose_name='is_active', default=True)
    mobile = models.TextField(verbose_name='bank_code', null=True, blank=True)
    objects =  UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        db_table = 'user'

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name


class UserSession(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    session = models.OneToOneField(Session, on_delete=models.CASCADE)

    class Meta:
        db_table = 'user_session'

class Participants(models.Model):
    biccode = models.CharField(max_length=255)
    sortcode = models.CharField(max_length=255)
    par_name = models.CharField(max_length=255)
    par_address = models.TextField()
    settlement_acct = models.CharField(max_length=255)
    class Meta:
        db_table = 'participants'

class Transaction_Detail(models.Model):
    send_bank_code = models.CharField(max_length=255)
    txn_ref = models.CharField(max_length=255)
    batch_no = models.CharField(max_length=255)
    benef_acct_no = models.CharField(max_length=255)
    benef_name = models.CharField(max_length=255)
    benef_bank_code = models.CharField(max_length=255)
    benef_bank_sort_code = models.CharField(max_length=255)
    txn_amt = models.FloatField()
    txn_desc = models.TextField()
    txn_value_date = models.TextField()
    process_date = models.DateTimeField(auto_now=True)
    status = models.TextField(default="pending")
    re_submit_date_time = models.TextField(null=True, blank=True)
    tnx_type = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'transaction_detail'


class Payment_Type(models.Model):
    types = models.TextField()
    file_type = models.TextField()

    class Meta:
        db_table = 'payment_type'


class Config(models.Model):
    name = models.TextField(null=True, blank=True)
    action = models.TextField(default='10')

    class Meta:
        db_table = 'config'


class Mapping(models.Model):
    name = models.TextField(null=True, blank=True)
    code = models.TextField(null=True, blank=True)
    para = models.JSONField()

    class Meta:
        db_table = 'mapping'


class Schema(models.Model):
    pac_name = models.TextField(null=True, blank=True)
    schemas = models.TextField(null=True, blank=True)
    date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'schema'



class Payment(models.Model):
    ptype = models.TextField()
    payee_id = models.TextField(null=True, blank=True)
    employee_name = models.TextField(null=True, blank=True)
    bnkcode = models.TextField(null=True, blank=True)
    bank_name = models.TextField(null=True, blank=True)
    acctno = models.TextField(null=True, blank=True)
    currency = models.TextField(null=True, blank=True)
    gross_amount = models.FloatField(null=True, blank=True)
    tax_amount = models.FloatField(null=True, blank=True)
    amount = models.FloatField()
    batctno = models.TextField(null=True, blank=True)
    date = models.DateField(auto_now=True)
    desc = models.TextField(null=True, blank=True)
    status = models.TextField(default="created")
    response = models.TextField(default='waiting for response')

    class Meta:
        db_table = 'payment'


class SinglePayment(models.Model):
    batch_id = models.TextField(null=True, blank=True)
    sname = models.TextField(null=True, blank=True)
    saddress = models.TextField(null=True, blank=True)
    s_db_acc = models.TextField(null=True, blank=True)
    rname = models.TextField(null=True, blank=True)
    raddress = models.TextField(null=True, blank=True)
    r_cr_acc = models.TextField(null=True, blank=True)
    rbank = models.TextField(null=True, blank=True)
    amount = models.FloatField(null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    date_created = models.DateTimeField(auto_now=True)
    status = models.TextField(null=True, blank=True)
    response = models.TextField(default='waiting for response')

    class Meta:
        db_table = 'single_payment'

class ProcessBatch(models.Model):
    batch_id = models.TextField(null=True, blank=True)
    tnxtype = models.TextField(null=True, blank=True)
    date = models.TextField(null=True, blank=True)
    status = models.TextField(default='created')
    author = models.TextField(null=True, blank=True)
    total_amt = models.FloatField(null=True, blank=True)
    num_of_trans = models.TextField(null=True, blank=True)
    response = models.TextField(default='waiting for response')

    class Meta:
        db_table = 'process_batch'


class SettlementAccount(models.Model):
    biccode = models.CharField(max_length=255)
    cur = models.TextField(null=True, blank=True)
    set_acct_no = models.TextField(null=True, blank=True)
    date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'settlement_account'


class File_Log(models.Model):
    filename = models.TextField(null=True, blank=True)
    uploader = models.TextField(null=True, blank=True)
    date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'file_log'


class File_Management(models.Model):
    batch_id = models.TextField(null=True, blank=True)
    filename = models.TextField(null=True, blank=True)
    date_created = models.DateTimeField(auto_now=True)
    status = models.TextField(default='created')

    class Meta:
        db_table = 'file_management'


class WorkingDays(models.Model):
    days = models.TextField(null=True, blank=True)
    day_code = models.TextField(null=True, blank=True)
    status = models.TextField(default='1')

    class Meta:
        db_table = 'working_day'


class ResponseMessage(models.Model):
    code = models.TextField(null=True, blank=True)
    message = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'response_massage'



class MessageId(models.Model):
    msgname = models.TextField(null=True, blank=True)
    msgid = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'message_id'