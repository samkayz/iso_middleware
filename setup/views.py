from typing import Counter
from xml.etree.ElementTree import XML
from django.db.models.aggregates import Count
from django.shortcuts import render, redirect
from rest_framework import status
from function import *
from django.views.decorators.http import require_http_methods
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.dispatch.dispatcher import receiver
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.core import serializers
from django.http import JsonResponse
Objects = Main()
import io
import csv
import ast
import uuid
import math
import datetime
import random
import string
import os
import openpyxl
import lxml.etree as ET
from create_xml import *
# import xml.etree.ElementTree as ET

script_dir = os.path.dirname(os.path.abspath(__file__))
dest_dir = os.path.join('static/out')
newpath = os.path.join('static/in')
atsPath = os.path.join('static/ats')

createXMLFile = CreateXMLS()

@receiver(user_logged_in)
def remove_other_sessions(sender, user, request, **kwargs):
    # remove other sessions
    Session.objects.filter(usersession__user=user).delete()

    # save current session
    request.session.save()

    # create a link from the user to the current session (for later removal)
    UserSession.objects.get_or_create(
        user=user,
        session=Session.objects.get(pk=request.session.session_key)
    )

def login(request):
    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']
        return Objects.Login(request, email, password)
    else:
        return render(request, 'login.html')


@login_required(login_url='/')
def home(request):
    active = "true"
    totalTranx = Payment.objects.all().count()
    show = Objects.Show_Log_With_limit()
    trans = Objects.Show_Transact_With_Limit()
    total = Objects.total_trans()
    round_total = round(total, 2)
    files = Objects.Get_File_Name()
    l = []
    for file in files:
        # print(file)
        l.append(file)
    lenght = len(l)
    return render(request, 'index.html', {'home': active, 'totalTranx': totalTranx, 'show': show, 'trans': trans, 
    'total': round_total, 'files': files, 'lenght': lenght})


def logout(request):
    return Objects.Logout(request)


@require_POST
@csrf_exempt
def notify(request):
    body = Objects.Notification(request)
    return body


@require_POST
@csrf_exempt
def paydirect(request):
    body = Objects.pay(request)
    return body


@login_required(login_url='/')
def process(request):
    active = "true"
    show = Objects.Transaction()
    return render(request, 'process.html', {'show': show, 'active': active})


@login_required(login_url='/')
def add_participant(request):
    if request.method == "POST":
        biccode = request.POST['biccode']
        sortcode = request.POST['sortcode']
        par_name = request.POST['par_name']
        par_address = request.POST['par_address']
        settlement_acct = request.POST['settlement_acct']

        action = Objects.AddParticipant(request, biccode, sortcode, par_name, par_address, settlement_acct)
        return action
    else:
        par = "true"
        return render(request, 'add_participant.html', {'par': par})


@login_required(login_url='/')
def all_participant(request):
    par = "true"
    show = Objects.All_Participant()
    return render(request, 'all_participant.html', {'par': par, 'show': show})


@login_required(login_url='/')
def updateparticipant(request, id):
    if request.method == "POST":
        biccode = request.POST['biccode']
        sortcode = request.POST['sortcode']
        par_name = request.POST['par_name']
        par_address = request.POST['par_address']
        settlement_acct = request.POST['settlement_acct']
        resp = Objects.UpdateParticipant(request, id, biccode, sortcode, par_name, par_address, settlement_acct)
        return resp
    else:
        show = Participants.objects.all().get(id=id)
        return render(request, 'updateparticipant.html', {'show':show})


@login_required(login_url='/')
def deleteparticipant(request, biccode):
    resp = Objects.DeleteParticipant(biccode)
    return resp


@login_required(login_url='/')
def config(request):
    if request.method == "POST":
        name = request.POST['name']
        action = request.POST['action']
        resp = Objects.conf(request, name, action)
        return resp
    else:
        sTime = Objects.Configuration(name='mbroke')
        ctime = Objects.Configuration(name='cuttoff')
        serv = Objects.Configuration(name='service')
        biccode = Objects.Configuration(name='biccode')

        sun = Objects.showWorkingDays(days='Sunday')
        mon = Objects.showWorkingDays(days='Monday')
        tue = Objects.showWorkingDays(days='Tuesday')
        wed = Objects.showWorkingDays(days='Wednesday')
        thur = Objects.showWorkingDays(days='Thursday')
        fri = Objects.showWorkingDays(days='Friday')
        sat = Objects.showWorkingDays(days='Saturday')
        return render(request, 'config.html', {'sTime': sTime, 'ctime': ctime, 'serv': serv, 
        'sun':sun, 'mon':mon, 'tue':tue, 'wed':wed, 'thur':thur, 'fri':fri, 'sat':sat, 'biccode': biccode})


@login_required(login_url='/')
def mapping(request):
    if request.method == "POST":
        para = request.POST['para']
        name = request.POST['name']
        code = request.POST['code']
        resp = Objects.addMapping(request, code, name, ast.literal_eval(para))
        return resp
    else:
        return render(request, 'mapping.html')


@login_required(login_url='/')
def allmapping(request):
    show = Objects.ShowAllMapping()
    # dic = Mapping.objects.all().get(id=5)

    # print(dic.para['key'])
    return render(request, 'allmapping.html', {'show': show})


@login_required(login_url='/')
def addSchema(request):
    if request.method == 'POST':
        pac_name = request.POST['pac_name']
        schemas = request.POST['schemas']
        resp = Objects.add_Schema(request, pac_name=pac_name, schemas=schemas)
        return resp
    else:
        return render(request, 'schema.html')


@login_required(login_url='/')
def allSchema(request):
    show = Objects.ShowAllSchema()
    return render(request, 'allschema.html', {'show': show})


@login_required(login_url='/')
def editSchema(request, id):
    allData = Objects.edit_Schema(id)
    return render(request, 'edit_schema.html', {'allData': allData})


@login_required(login_url='/')
def update_schema(request):
    if request.method == "POST":
        sid = request.POST['id']
        pac_name = request.POST['pac_name']
        schemas = request.POST['schemas']

        resp = Objects.updateSchema(request, sid, pac_name, schemas)
        return resp


@login_required(login_url='/')
def addAccount(request, biccode):
    return render(request, 'add_account.html', {'biccode': biccode})


@login_required(login_url='/')
def add_account(request):
    if request.method == "POST":
        biccode = request.POST['biccode']
        settlement_acct = request.POST['settlement_acct']
        cur = request.POST['cur']

        if SettlementAccount.objects.filter(set_acct_no=settlement_acct).exists():
            data = {
                "message": "Fail!!! Accounts already added"
            }
            return JsonResponse(data)
        else:
            Objects.addSettlementAccount(biccode, cur, settlement_acct)

            data = {
                "message": "Success!!! Accounts Added Succesfully"
            }
            return JsonResponse(data)


@login_required(login_url='/')
def transactions(request):
    show = Objects.Transact()
    Lim = Objects.Show_Transact_With_Limit()
    reject = Payment.objects.filter(status='rejected')
    # check = Objects.Check_Transaction_status()
    return render(request, 'transactions.html', {'show': show, 'Lim': Lim, 'reject':reject})


@login_required(login_url='/')
def veiwTrans(request, batch_id):
    show = Objects.ViewTransact(batch_id)[0]
    total_trans = show.count()
    msg = "TRANSACTION"
    return render(request, 'view_trans.html', {'show': show, 'count':total_trans, 'msg':msg})



@login_required(login_url='/')
def veiwReject(request, batch_id):
    show = Objects.ViewTransact(batch_id)[1]
    total_reject = show.count()
    msg = "REJECTED"
    return render(request, 'view_trans.html', {'show': show, 'count':total_reject, 'msg':msg})

@login_required(login_url='/')
def upload(request):
    # identifier = request.user.identifier
    # bicode = Participants.objects.values('biccode').get(identifier=identifier)['biccode']
    # settl_acct = Participants.objects.values('settlement_acct').get(identifier=identifier)['settlement_acct']
    # per_name = Participants.objects.values('par_name').get(identifier=identifier)['par_name']
    loggedin_user = request.user.fullname
    
    today = datetime.datetime.now()
    d = datetime.datetime.now().replace(tzinfo=datetime.timezone.utc)
    no_ms = d.replace(microsecond=0)
    now = no_ms.isoformat()
    tnow = today.strftime('%Y-%m-%d')
    N = 8
    nref = ''.join(random.choices(string.digits, k=N))
    bcode = str(nref)

    U = 7
    ref = ''.join(random.choices(string.digits, k=U))
    txn = str(ref)
    txnref = f'FT{txn}'

    current_time = today.strftime('%H:%M')
    
    if request.method == "POST":
        csv_file = request.FILES['file']
        rec_type = request.POST['tnx_type']
        filename =  csv_file.name.split('.')[0]
        print(filename)
        if Objects.Check_File_Created(filename) == True:
            messages.error(request, 'File Already Uploaded. Please Upload another file or rename the current file')
            return redirect('/upload')
        elif not csv_file.name.endswith('.csv'):
            messages.error(request, 'File not csv')
            return redirect('/upload')
        else:
            decoded_file = csv_file.read().decode('utf-8')
            io_string = io.StringIO(decoded_file)


            lists = []
            total_sum = []
            for data in csv.reader(io_string, delimiter=',', quotechar='|'):
                L = 10
                pid = ''.join(random.choices(string.digits, k=L))
                payee_id = str(pid)
                data_dict = {}
                data_dict['pay_id'] = payee_id
                data_dict['emp_name'] = data[0]
                data_dict['ben_bicode'] = data[1]
                data_dict['ben_bank_name'] = data[2]
                data_dict['acct_no'] = data[3]
                data_dict['pay_date'] = tnow
                data_dict['currency'] = data[4]
                data_dict['g_amt'] = data[5]
                data_dict['tax_amt'] = data[6]
                data_dict['amount'] = data[7]
                data_dict['desc'] = data[8]

                total_sum.append(float(data[7]))
                total_sum.append(float(data[6]))
                total_sum.append(float(data[5]))
                ben_bicode = data[1]
                lists.append(data_dict)
                # if Objects.check_availability(ben_bicode) == False:
                #     messages.success(request, f'{ben_bicode} is not a valid biccode')
                #     return redirect('/upload')
            total_record = (len(lists))
            total = (sum(total_sum))
            if Objects.cut_Of_Time() == False:
                for k in lists:
                    # print(k)
                    pay_id = k['pay_id']
                    emp_name = k['emp_name']
                    ben_bicode = k['ben_bicode']
                    ben_bank_name = k['ben_bank_name']
                    acct_no = k['acct_no']
                    currency = k['currency']
                    g_amt = k['g_amt']
                    tax_amt = k['tax_amt']
                    amount = k['amount']
                    desc = k['desc']
                    if Objects.check_availability(ben_bicode) == False:
                        messages.error(request, f'{ben_bicode} is not a valid biccode or does not exist')
                        return redirect('/upload')
                    else:
                        Objects.pay(request, rec_type, pay_id, emp_name, ben_bicode, ben_bank_name, acct_no, currency, g_amt, tax_amt, amount, bcode, desc, status='queue')
                Objects.Create_File_Log(filename, loggedin_user)
                pbatch = ProcessBatch(batch_id=bcode, tnxtype=rec_type, total_amt=total, num_of_trans=total_record, author=loggedin_user, date=now, status='queue')
                pbatch.save()
                messages.success(request, "Cut of Time!! It will be Processed")
                messages.success(request, f'You Pay Load has been accepted and your BATCH NO is {bcode}. It will be processed at due time')
                return redirect('/upload')
            elif Objects.GetWorkingDays() == False:
                for k in lists:
                    # print(k)
                    pay_id = k['pay_id']
                    emp_name = k['emp_name']
                    ben_bicode = k['ben_bicode']
                    ben_bank_name = k['ben_bank_name']
                    acct_no = k['acct_no']
                    currency = k['currency']
                    g_amt = k['g_amt']
                    tax_amt = k['tax_amt']
                    amount = k['amount']
                    desc = k['desc']
                    if Objects.check_availability(ben_bicode) == False:
                        messages.error(request, f'{ben_bicode} is not a valid biccode or does not exist')
                        return redirect('/upload')
                    else:
                        Objects.pay(request, rec_type, pay_id, emp_name, ben_bicode, ben_bank_name, acct_no, currency, g_amt, tax_amt, amount, bcode, desc, status='queue')
                Objects.Create_File_Log(filename, loggedin_user)
                pbatch = ProcessBatch(batch_id=bcode, tnxtype=rec_type, total_amt=total, num_of_trans=total_record, author=loggedin_user, date=now, status='queue')
                pbatch.save()
                messages.success(request, "Out of Working Hours")
                messages.success(request, f'You Pay Load has been accepted and your BATCH NO is {bcode}. It will be processed at due time.')
                return redirect('/upload')
            else:
                createXMLFile.newXML(request, bcode, now, total_record, total, tnow, lists, rec_type, txnref, filename, loggedin_user, total)
                return redirect('/upload')
    else:
        return render(request, 'upload.html')


@login_required(login_url='/')
def log(request):
    show = Objects.ShowLog()
    return render(request, 'log.html', {'show': show})


@login_required(login_url='/')
def all_account(request):
    show = Objects.Show_Settlement()
    return render(request, 'part_acct.html', {'show': show})


@login_required(login_url='/')
def delete_schema(request, id):
    resp = Objects.Delete_Schema(id)
    return resp

@login_required(login_url='/')
def create_file(request):
    if request.method == "POST":
        payee_id = request.POST['payee_id']
        bnkcode = request.POST['bnkcode']
        currency = request.POST['currency']
        acctno = request.POST['acctno']
        bank_name = request.POST['bank_name']
        amount = request.POST['amount']
        employee_name = request.POST['employee_name']
        gross_amount = request.POST['gross_amount']
        tax_amount = request.POST['tax_amount']
        batctno = request.POST['batctno']

        resp = createXMLFile.create_xml(request,batctno, amount, payee_id, currency, acctno, employee_name, bnkcode, gross_amount, tax_amount)
        return resp


@login_required(login_url='/')
def filelog(request):
    show = Objects.ViewLog().order_by('-id')
    return render(request, 'filelog.html', {'show': show})


@login_required(login_url='/')
def add_day(request):
    if request.method == "POST":
        day = request.POST['day']
        days = Objects.GetDays(day)
        Objects.Add_Days(days, day)
        # print(days)
        # print(day)
        return redirect('/config')



@login_required(login_url='/')
def single(request):
    N = 8
    nref = ''.join(random.choices(string.digits, k=N))
    bcode = str(nref)

    U = 7
    ref = ''.join(random.choices(string.digits, k=U))
    txn = str(ref)
    txnref = f'FT{txn}'
    loggedin_user = request.user.fullname
    filename = "single"
    d = datetime.datetime.now().replace(tzinfo=datetime.timezone.utc)
    no_ms = d.replace(microsecond=0)
    now = no_ms.isoformat()

    if request.method == "POST":
        sname = request.POST['sname']
        saddress = request.POST['saddress']
        s_db_acc = request.POST['s_db_acc']
        rname = request.POST['rname']
        raddress = request.POST['raddress']
        r_cr_acc = request.POST['r_cr_acc']
        rbank = request.POST['rbank']
        amount = request.POST['amount']
        fmount = float(amount)
        comment = request.POST['comment']
        if rbank == "":
            messages.error(request, "Please Select Reciever's bank")
            return render(request, 'single.html', {'sname':sname, 'saddress':saddress, 's_db_acc':s_db_acc, 
            'rname':rname, 'raddress':raddress, 'r_cr_acc':r_cr_acc, 'amount':fmount, 'comment':comment})
        else:
            if Objects.cut_Of_Time() == False:
                Objects.Create_File_Log(filename, loggedin_user)
                pbatch = ProcessBatch(batch_id=bcode, tnxtype='single', total_amt=amount, num_of_trans='1', author=loggedin_user, date=now, status='queue')
                pbatch.save()
                Objects.CreateSinglePayment(bcode, sname, saddress, s_db_acc, rname, raddress, r_cr_acc, rbank, amount, comment, status='queue')
                messages.success(request, f'Operation out of Working Hour. Your Transaction will be processed during the working Day. Your batch code is {bcode}')
                return redirect('/single')
            elif Objects.GetWorkingDays() == False:
                Objects.Create_File_Log(filename, loggedin_user)
                pbatch = ProcessBatch(batch_id=bcode, tnxtype='single', total_amt=amount, num_of_trans='1', author=loggedin_user, date=now, status='queue')
                pbatch.save()
                Objects.CreateSinglePayment(bcode, sname, saddress, s_db_acc, rname, raddress, r_cr_acc, rbank, amount, comment, status='queue')
                messages.success(request, f'Operation out of Working days. Your Transaction will be processed during the working Day. Your batch code is {bcode}')
                return redirect('/single')
            else:
                createXMLFile.SingleXML(amount, comment, sname, saddress, s_db_acc, rbank, rname, raddress, r_cr_acc, bcode, txnref)
                Objects.Create_File_Log(filename, loggedin_user)
                pbatch = ProcessBatch(batch_id=bcode, tnxtype='single', total_amt=amount, num_of_trans='1', author=loggedin_user, date=now, status='created')
                pbatch.save()
                Objects.CreateSinglePayment(bcode, sname, saddress, s_db_acc, rname, raddress, r_cr_acc, rbank, fmount, comment, status='created')
                Objects.Create_File_Management(bcode, txnref)
                messages.success(request, f'Operation Successful. Your batch code is {bcode}')
                return redirect('/single')
    else: 
        show = Objects.All_Participant()
        return render(request, 'single.html', {'show':show})

@login_required(login_url='/')
def b2b(request):
    N = 8
    nref = ''.join(random.choices(string.digits, k=N))
    bcode = str(nref)

    U = 7
    ref = ''.join(random.choices(string.digits, k=U))
    txn = str(ref)
    txnref = f'FT{txn}'
    loggedin_user = request.user.fullname
    filename = "b2b"
    d = datetime.datetime.now().replace(tzinfo=datetime.timezone.utc)
    no_ms = d.replace(microsecond=0)
    now = no_ms.isoformat()

    if request.method == "POST":
        sname = request.POST['sname']
        saddress = request.POST['saddress']
        s_db_acc = request.POST['s_db_acc']
        rname = request.POST['rname']
        raddress = request.POST['raddress']
        r_cr_acc = request.POST['r_cr_acc']
        rbank = request.POST['rbank']
        amount = request.POST['amount']
        fmount = float(amount)
        comment = request.POST['comment']
        if rbank == "":
            messages.error(request, "Please Select Reciever's bank")
            return render(request, 'single.html', {'sname':sname, 'saddress':saddress, 's_db_acc':s_db_acc, 
            'rname':rname, 'raddress':raddress, 'r_cr_acc':r_cr_acc, 'amount':fmount, 'comment':comment})
        else:
            if Objects.cut_Of_Time() == False:
                Objects.Create_File_Log(filename, loggedin_user)
                pbatch = ProcessBatch(batch_id=bcode, tnxtype='b2b', total_amt=amount, num_of_trans='1', author=loggedin_user, date=now, status='queue')
                pbatch.save()
                Objects.CreateSinglePayment(bcode, sname, saddress, s_db_acc, rname, raddress, r_cr_acc, rbank, amount, comment, status='queue')
                messages.success(request, f'Operation out of Working Hour. Your Transaction will be processed during the working Day. Your batch code is {bcode}')
                return redirect('/b2b')
            elif Objects.GetWorkingDays() == False:
                Objects.Create_File_Log(filename, loggedin_user)
                pbatch = ProcessBatch(batch_id=bcode, tnxtype='b2b', total_amt=amount, num_of_trans='1', author=loggedin_user, date=now, status='queue')
                pbatch.save()
                Objects.CreateSinglePayment(bcode, sname, saddress, s_db_acc, rname, raddress, r_cr_acc, rbank, amount, comment, status='queue')
                messages.success(request, f'Operation out of Working days. Your Transaction will be processed during the working Day. Your batch code is {bcode}')
                return redirect('/b2b')
            else:
                createXMLFile.SingleXML(amount, comment, sname, saddress, s_db_acc, rbank, rname, raddress, r_cr_acc, bcode, txnref)
                Objects.Create_File_Log(filename, loggedin_user)
                pbatch = ProcessBatch(batch_id=bcode, tnxtype='b2b', total_amt=amount, num_of_trans='1', author=loggedin_user, date=now, status='created')
                pbatch.save()
                Objects.CreateSinglePayment(bcode, sname, saddress, s_db_acc, rname, raddress, r_cr_acc, rbank, fmount, comment, status='created')
                Objects.Create_File_Management(bcode, txnref)
                messages.success(request, f'Operation Successful. Your batch code is {bcode}')
                return redirect('/b2b')
    else: 
        show = Objects.All_Participant()
        return render(request, 'b2b.html', {'show':show})

@login_required(login_url='/')
def single_trans(request):
    show = Objects.showSingleTrans()[0]
    shows = Objects.showSingleTrans()[1]
    return render(request, "single_trans.html", {'show':show, 'shows':shows})


@login_required(login_url='/')
def users(request):
    showUser = Objects.getUser()
    return render(request, 'user.html', {'showUser': showUser})


@login_required(login_url='/')
def userAction(request, action, id):
    if action == "activate":
        Objects.activateUser(id)
        return redirect('/users')
    else:
        Objects.DeactivateUser(id)
        return redirect('/users')


@login_required(login_url='/')
def adduser(request):
    if request.method == "POST":
        fullname = request.POST['fullname']
        role = request.POST['role']
        email = request.POST['email']
        password = request.POST['password']
        resp = Objects.CreateUser(request, fullname, email, password, role)
        return resp
    else:
        return render(request, 'adduser.html')


@login_required(login_url='/')
def updateuser(request, id):
    if request.method == "POST":
        fullname = request.POST['fullname']
        email = request.POST['email']

        resp = Objects.UpdateUser(request, id, fullname, email)
        return resp
    else:
        show = User.objects.all().get(id=id)
        return render(request, 'updateuser.html', {'show':show})


@login_required(login_url='/')
def deleteuser(request, id):
    resp = Objects.DeleteUser(id)
    return resp


@login_required(login_url='/')
def addmsg(request):
    if request.method == "POST":
        code = request.POST['code']
        msg = request.POST['msg']

        resp = Objects.AddResponseMessage(request, msg, code)
        return resp
    else:
        return render(request, 'addmsg.html')


@login_required(login_url='/')
def allmsgs(request):
    show = Objects.ShowAllMessages()
    return render(request, 'allmsgs.html', {'show':show})


@login_required(login_url='/')
def updatemsg(request, id):
    if request.method == "POST":
        codes = request.POST['code']
        msg = request.POST['msg']
        resp = Objects.UpdateMessages(request, id, codes, msg)
        return resp
    else:
        show = Objects.get_all_message(id)
        return render(request, 'updatemsg.html', {'show':show})


@login_required(login_url='/')
def deletemsg(request, id):
    resp = Objects.DeleteMessage(id)
    return resp


@login_required(login_url='/')
def addmsgid(request):
    if request.method == "POST":
        msgname = request.POST['msgname']
        msgid = request.POST['msgid']

        resp = Objects.AddMSGId(request, msgname, msgid)
        return resp
    
    else:
        return render(request, "add_msgid.html")

@login_required(login_url='/')
def allmsgid(request):
    show = Objects.AllMSGId()
    return render(request, "all_msgid.html", {'show': show})


@login_required(login_url='/')
def deletemsgid(request, id):
    resp = Objects.DeleteMSGId(id)
    return resp


@login_required(login_url='/')
def updatemsgid(request, id):
    if request.method == "POST":
        msgid = request.POST['msgid']

        resp = Objects.UpdateMSGId(request, id, msgid)
        return resp
    else:
        show = MessageId.objects.all().get(id=id)
        return render(request, "updatemsgid.html", {'show': show})