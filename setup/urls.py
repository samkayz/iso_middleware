"""setup URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from os import name
from django.urls import path, include
from . import views

urlpatterns = [
    path('console/', include('console.urls')),
    path('api/v1/', include('api.urls')),
    path('', views.login, name='login'),
    path('home', views.home, name='home'),
    path('logout', views.logout, name='logout'),
    path('notify', views.notify, name='notify'),
    path('process', views.process, name='process'),
    path('add_participant', views.add_participant, name="add_participant"),
    path('all_participant', views.all_participant, name='all_participant'),
    path('updateparticipant/<id>', views.updateparticipant, name='updateparticipant'),
    path('deleteparticipant/<biccode>', views.deleteparticipant, name='deleteparticipant'),
    path('config', views.config, name='config'),
    path('mapping', views.mapping, name='mapping'),
    path('allmapping', views.allmapping, name='allmapping'),
    path('addSchema', views.addSchema, name='addSchema'),
    path('allSchema', views.allSchema, name='allSchema'),
    path('editSchema/<id>', views.editSchema, name='editSchema'),
    path('update_schema', views.update_schema, name='update_schema'),
    path('paydirect', views.paydirect, name='paydirect'),
    path('addAccount/<biccode>', views.addAccount, name='addAccount'),
    path('add_account', views.add_account, name='add_account'),
    path('transactions', views.transactions, name='transactions'),
    path('veiwTrans/<batch_id>', views.veiwTrans, name='veiwTrans'),
    path('veiwReject/<batch_id>', views.veiwReject, name='veiwReject'),
    path('upload', views.upload, name='upload'),
    path('log', views.log, name='log'),
    path('all_account', views.all_account, name='all_account'),
    path('delete_schema/<id>', views.delete_schema, name='delete_schema'),
    path('create_file', views.create_file, name='create_file'),
    path('filelog', views.filelog, name='filelog'),
    path('add_day', views.add_day, name='add_day'),
    path('single', views.single, name='single'),
    path('single_trans', views.single_trans, name='single_trans'),
    path('users', views.users, name='users'),
    path('userAction/<action>/<id>', views.userAction, name='userAction'),
    path('adduser', views.adduser, name='adduser'),
    path('updateuser/<id>', views.updateuser, name='updateuser'),
    path('deleteuser/<id>', views.deleteuser, name='deleteuser'),
    path('addmsg', views.addmsg, name='addmsg'),
    path('allmsgs', views.allmsgs, name='allmsgs'),
    path('updatemsg/<id>', views.updatemsg, name='updatemsg'),
    path('deletemsg/<id>', views.deletemsg, name='deletemsg'),
    path('addmsgid', views.addmsgid, name='addmsgid'),
    path('allmsgid', views.allmsgid, name='allmsgid'),
    path('deletemsgid/<id>', views.deletemsgid, name='deletemsgid'),
    path('updatemsgid/<id>', views.updatemsgid, name='updatemsgid'),
    path('b2b', views.b2b, name='b2b'),
]
