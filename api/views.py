from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.response import Response
from django.db.models import Q
from .serializers import *
import random
import string
# import xml.etree.ElementTree as ET
import lxml.etree as ET
from create_xml import *
import uuid
import math
from function import *
import datetime
from django.contrib.auth import user_logged_in
from django.contrib.auth import get_user_model
from function import *
from django.db.models import Sum
User = get_user_model()
Iso = Main()
import os
Objects = Main()
createXMLFile = CreateXMLS()
script_dir = os.path.dirname(os.path.abspath(__file__))
dest_dir = os.path.join('out')
newpath = os.path.join('in')
atsPath = os.path.join('ats')


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def paydirect(request):
    # identifier = request.user.identifier
    # bicode = Participants.objects.values('biccode').get(identifier=identifier)['biccode']
    # settl_acct = Participants.objects.values('settlement_acct').get(identifier=identifier)['settlement_acct']
    # per_name = Participants.objects.values('par_name').get(identifier=identifier)['par_name']
    loggedin_user = request.user.fullname
    today = datetime.datetime.now()
    d = datetime.datetime.now().replace(tzinfo=datetime.timezone.utc)
    no_ms = d.replace(microsecond=0)
    now = no_ms.isoformat()
    tnow = today.strftime('%Y-%m-%d')
    N = 8
    nref = ''.join(random.choices(string.digits, k=N))
    bcode = str(nref)

    U = 7
    ref = ''.join(random.choices(string.digits, k=U))
    txn = str(ref)
    txnref = f'FT{txn}'

    loggedin_user = request.user.fullname
    tnx_type = request.data.get('tnx_type')
    processId = request.data.get('processId')
    transactionList = request.data.get('transactionList')
    no_of_tnx = len(transactionList)

    total_sum = []
    biccode = []
    if Objects.Check_File_Created(processId) == True:
        resp = {
                "code": status.HTTP_403_FORBIDDEN,
                "status": "rejected",
                "reason": f'Transaction with ID: {processId} has been created already',
                "author": loggedin_user

            }
        return Response(data=resp, status=status.HTTP_200_OK)
    else:
        for i in transactionList:
            amount = i['amount']
            ben_bicode = i['ben_bicode']
            amt = float(amount)
            total_sum.append(amt)

            if Objects.check_availability(ben_bicode) == False:
                resp = {
                    "code": status.HTTP_200_OK,
                    "status": "rejected",
                    "reason": f'{ben_bicode} is not a valid biccode',
                    "author": loggedin_user

                }
                return Response(data=resp, status=status.HTTP_200_OK)
        total = (sum(total_sum))
        if Objects.cut_Of_Time() == False:
            for k in transactionList:
                L = 10
                pid = ''.join(random.choices(string.digits, k=L))
                payee_id = str(pid)
                pay_id = payee_id
                emp_name = k['emp_name']
                ben_bicode = k['ben_bicode']
                ben_bank_name = k['ben_bank_name']
                acct_no = k['acct_no']
                currency = k['currency']
                g_amt = k['g_amt']
                tax_amt = k['tax_amt']
                amount = k['amount']
                desc = k['desc']
                if Objects.check_availability(ben_bicode) == False:
                    resp = {
                        "code": status.HTTP_200_OK,
                        "status": "rejected",
                        "reason": f'{ben_bicode} is not a valid biccode',
                        "author": loggedin_user

                    }
                    return Response(data=resp, status=status.HTTP_200_OK)
                else:
                    Objects.pay(request, tnx_type, pay_id, emp_name, ben_bicode, ben_bank_name, acct_no, currency, g_amt, tax_amt, amount, bcode, desc, status='queue')
            pbatch = ProcessBatch(batch_id=bcode, tnxtype=tnx_type, total_amt=total, num_of_trans=no_of_tnx, author=loggedin_user, date=now, status='queue')
            pbatch.save()
            Objects.Create_File_Log(processId, loggedin_user)
            resp = {
                "totalTran": no_of_tnx,
                "batchNo": bcode,
                "code": status.HTTP_200_OK,
                "status": "accepted",
                "message": "Your Transaction is Out of time. It will be process in due time",
                "author": loggedin_user

            }
            return Response(data=resp, status=status.HTTP_200_OK)
        elif Objects.GetWorkingDays() == False:
            for k in transactionList:
                L = 10
                pid = ''.join(random.choices(string.digits, k=L))
                payee_id = str(pid)
                pay_id = payee_id
                emp_name = k['emp_name']
                ben_bicode = k['ben_bicode']
                ben_bank_name = k['ben_bank_name']
                acct_no = k['acct_no']
                currency = k['currency']
                g_amt = k['g_amt']
                tax_amt = k['tax_amt']
                amount = k['amount']
                desc = k['desc']
                if Objects.check_availability(ben_bicode) == False:
                    resp = {
                        "code": status.HTTP_200_OK,
                        "status": "rejected",
                        "reason": f'{ben_bicode} is not a valid biccode',
                        "author": loggedin_user

                    }
                    return Response(data=resp, status=status.HTTP_200_OK)
                else:
                    Objects.pay(request, tnx_type, pay_id, emp_name, ben_bicode, ben_bank_name, acct_no, currency, g_amt, tax_amt, amount, bcode, desc, status='queue')
            pbatch = ProcessBatch(batch_id=bcode, tnxtype=tnx_type, total_amt=total, num_of_trans=no_of_tnx, author=loggedin_user, date=now, status='queue')
            pbatch.save()
            Objects.Create_File_Log(processId, loggedin_user)
            resp = {
                "totalTran": no_of_tnx,
                "batchNo": bcode,
                "code": status.HTTP_200_OK,
                "status": "accepted",
                "message": "Your Transaction is Out of Working Days. It will be process in due time",
                "author": loggedin_user

            }
            return Response(data=resp, status=status.HTTP_200_OK)
        else:
            createXMLFile.newXML(request, bcode, now, no_of_tnx, total, tnow, transactionList, tnx_type, txnref, processId, loggedin_user, total)

            resp = {
                "totalTran": no_of_tnx,
                "batchNo": bcode,
                "code": status.HTTP_200_OK,
                "status": "accepted",
                "author": loggedin_user

            }
            return Response(data=resp, status=status.HTTP_200_OK)
