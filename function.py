from django.http import request, response
from console.models import *
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import get_user_model, authenticate, login as dj_login, logout as s_logout
from django.contrib.auth import user_logged_in
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import json
import xml.etree.ElementTree as ET
User = get_user_model()
import os
import random
import string
import uuid
import datetime
import time
from django.db.models import Sum
from django.db.models import *

script_dir = os.path.dirname(os.path.abspath(__file__))
dest_dir = os.path.join(script_dir, 'out')
# dest_dir = os.path.join('out')
# newpath = os.path.join('in')
atsPath = os.path.join('static/ats')



def time_in_range(start, end, x):
    """Return true if x is in the range [start, end]"""
    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end

__name__ == "__main__"
class Main:

    ### Login Function ####
    def Login(self, request, email, password):
        if User.objects.filter(email=email).exists() == False:
            messages.error(request, "Account doesn't Exist in the System")
            response = redirect('/')
            return response
        elif User.objects.values('is_active').get(email=email)['is_active'] == False:
            messages.error(request, "Your Acount is locked. Kindly contact the system Administrator to reactivate Account")
            response = redirect('/')
            return response
        else:
            user = authenticate(email=email, password=password)
            if user is not None:
                dj_login(request, user)
                #request.session.set_expiry(1200)
                response = redirect('home')
                return response
            else:
                messages.error(request, 'Invalid Credentials')
                response = redirect('/')
                return response

    
    #### Logout Function
    def Logout(self, request):
        s_logout(request)
        messages.success(request, "Logout Successfully")
        response = redirect('/')
        return response

    
    #### Transaction Receiever

    def Notification(self, request):
        U = 14
        ref = ''.join(random.choices(string.digits, k=U))
        txnref = str(ref)


        request_json = request.body.decode('utf-8')
        body = json.loads(request_json)

        tnx_type = body['tnx_type']
        txnDesc = body['txnDesc']
        transList = body['transactionList']
        # bankId = body['bankId']
        # batchNo = body['batchNo']
        # benefAcctNo = body['benefAcctNo']
        # benefName = body['benefName']
        # benefBankCode = body['benefBankCode']
        # benefBankSortCode = body['benefBankSortCode']
        # txnAmount = body['txnAmount']
        # txnDesc = body['txnDesc']
        # txnValueDate = body['txnValueDate']

        no_of_tnx = len(transList)

        for i in transList:
            print(i)

            txn = Transaction_Detail(txn_ref=txnref, send_bank_code=i['bankId'], batch_no=i['batchNo'], benef_acct_no=i['benefAcctNo'], benef_name=i['benefName'], 
            benef_bank_code=i['benefBankCode'], benef_bank_sort_code=i['benefBankSortCode'], txn_amt=i['txnAmount'], txn_desc=txnDesc, txn_value_date=i['txnValueDate'], tnx_type=tnx_type)
            txn.save()

        data = {
            "totalTrasaction": no_of_tnx,
            "TxnId": txnref,
            "Message": "Your proccess is accepted",
            "status": "accepted"
        }
        return JsonResponse(data)
    

    def pay(self, request, ptype, payee_id, employee_name, bnkcode, bank_name, acctno, currency, gross_amount, tax_amount, amount, batctno, desc, status):
        
        spay = Payment(ptype=ptype, payee_id=payee_id, employee_name=employee_name, bnkcode=bnkcode, 
        bank_name=bank_name, acctno=acctno, currency=currency, gross_amount=gross_amount, tax_amount=tax_amount, amount=amount, 
        batctno=batctno, desc=desc, status=status)
        
        return spay.save()
    
    def Transaction(self):
        show = Transaction_Detail.objects.filter()
        return show
    


    def AddParticipant(self, request, biccode, sortcode, par_name, par_address, settlement_acct):
        U = 5
        code = ''.join(random.choices(string.digits, k=U))
        idnt = str(code)
        if Participants.objects.filter(biccode=biccode).exists():
            messages.error(request, 'Record already eixst')
            response = redirect('/add_participant')
            return response
        else:
            par = Participants(biccode=biccode, sortcode=sortcode, par_name=par_name, par_address=par_address, 
            settlement_acct=settlement_acct)
            par.save()
            messages.success(request, "Participant Added")
            return redirect('/add_participant')

    def AddMSGId(self, request, msgname, msgid):
        if msgname == "":
            messages.error(request, "Please Select Message Type")
            return redirect('/addmsgid')
        if MessageId.objects.filter(msgid__startswith=msgid).exists():
            messages.error(request, "Message ID already exists")
            return redirect('/addmsgid')
        else:
            msg = MessageId(msgname=msgname, msgid=msgid)
            msg.save()
            messages.success(request, "Message ID Added successfully")
            return redirect('/addmsgid')

    
    def AllMSGId(self):
        show = MessageId.objects.filter().order_by('-id')
        return show


    def DeleteMSGId(self, id):
        delte = MessageId.objects.get(id=id)
        delte.delete()
        return redirect('/allmsgid')

    
    def UpdateMSGId(self, request, id, msgid):
        updt = MessageId.objects.filter(id=id)
        updt.update(msgid=msgid)
        messages.success(request, "Message ID Updated")
        return redirect(f'/updatemsgid/{id}')

    def All_Participant(self):
        show = Participants.objects.filter()
        return show

    
    def UpdateParticipant(self, request, id, biccode, sortcode, par_name, par_address, settlement_acct):
        instance = Participants.objects.filter(id=id)
        instance.update(biccode=biccode, sortcode=sortcode, par_name=par_name, par_address=par_address, 
            settlement_acct=settlement_acct)
        messages.success(request, "Participant Updated")
        return redirect(f'/updateparticipant/{id}')

    
    def DeleteParticipant(self, biccode):
        part = Participants.objects.get(biccode=biccode)
        settl= SettlementAccount.objects.get(biccode=biccode)
        part.delete()
        settl.delete()
        return redirect('/all_participant')



    def conf(self, request, name, action):
        if Config.objects.filter(name=name).exists():
            Config.objects.filter(name=name).update(action=action)
            messages.success(request, "Successful")
            return redirect('/config')
        else:
            cn = Config(name=name, action=action)
            cn.save()
            messages.success(request, "Successful")
            return redirect('/config')

    
    def Configuration(self, name):
        try:
            s_conf = Config.objects.values('action').get(name=name)['action']
        except:
            s_conf = ''
        return s_conf

    
    def addMapping(self, request, code, name, para):
        add = Mapping(code=code, name=name, para=para)
        add.save()
        messages.success(request, "Added Successful")
        return redirect('/mapping')
    

    def ShowAllMapping(self):
        show = Mapping.objects.filter()
        return show

    
    def add_Schema(self, request, pac_name, schemas):
        sch = Schema(pac_name=pac_name, schemas=schemas)
        sch.save()
        messages.success(request, "Added Successful")
        return redirect('/addSchema')

    
    def ShowAllSchema(self):
        show = Schema.objects.filter()
        return show

    def edit_Schema(self, id):
        allData = Schema.objects.all().get(id=id)
        return allData

    
    def updateSchema(self, request, id, pac_name, schemas):
        Schema.objects.filter(id=id).update(pac_name=pac_name, schemas=schemas)
        messages.success(request, "Update Successful")
        return redirect('/allSchema')

    
    def addSettlementAccount(self, biccode, cur, set_acct_no):

        sett = SettlementAccount(biccode=biccode, cur=cur, set_acct_no=set_acct_no)
        return sett.save()


    def Transact(self):
        show = ProcessBatch.objects.filter(tnxtype='bulk').order_by('-id')
        return show

    
    def ViewTransact(self, batch_id):
        show = Payment.objects.filter(batctno=batch_id)
        reject = Payment.objects.filter(batctno=batch_id, status='rejected')
        count_sucess = Payment.objects.filter(batctno=batch_id, status='accepted')
        return show, reject

    def ShowLog(self):
        show = File_Log.objects.filter()
        return show

    
    def Show_Log_With_limit(self):
        show = File_Log.objects.filter().order_by('-id')[:9]
        return show


    def Show_Transact_With_Limit(self):
        show = ProcessBatch.objects.filter().order_by('-id')[:4]
        return show
    

    def Show_Settlement(self):
        show = SettlementAccount.objects.filter()
        return show

    def Delete_Schema(self, id):
        delt = Schema.objects.filter(id=id)
        delt.delete()
        return redirect('/allSchema')

    def update_response(self, OrgnlTxId, msg):
        if msg == "ACCP":
            Payment.objects.filter(payee_id=OrgnlTxId).update(status="accepted")
            pass
        elif msg == "ACSC":
            Payment.objects.filter(payee_id=OrgnlTxId).update(status="settled")
            pass
        elif msg == "RJCT":
            Payment.objects.filter(payee_id=OrgnlTxId).update(status="rejected")
            pass
        else:
            Payment.objects.filter(payee_id=OrgnlTxId).update(status="pending")
    

    def total_trans(self):
         total = Payment.objects.aggregate(Sum('amount'))['amount__sum']
         return total

    

    def check_availability(self, biccode):
        check = Participants.objects.filter(biccode=biccode).exists()
        return check


    def cut_Of_Time(self):
        xtime = Config.objects.values('action').get(name='cuttoff')['action']
        stime = xtime.split(':')
        shr = int(stime[0])
        smins = int(stime[1])
        today = datetime.datetime.now()
        current_time = today.strftime('%H:%M:%S')
        hr = today.strftime('%H')
        mins = today.strftime('%M')
        sec = today.strftime('%S')
        start = datetime.time(0, 0, 0)
        end = datetime.time(shr, smins)
        response = time_in_range(start, end, datetime.time(int(hr), int(mins), int(sec)))
        return response

    
    def Get_File_Name(self):
        files = os.listdir(atsPath)
        return files

    
    def Create_File_Management(self, batch_id, filename):
        create_file_log = File_Management(batch_id=batch_id, filename=filename)
        return create_file_log.save()
    
    def ViewLog(self):
        show = File_Management.objects.filter()
        return show
    
    def Check_File_Created(self, filename):
        check = File_Log.objects.filter(filename=filename).exists()
        return check

    def Create_File_Log(self, filename, loggedin_user):
        file_log = File_Log(filename=filename, uploader=loggedin_user)
        return file_log.save()
    

    def Check_Transaction_status(self, batchId):
        check = Payment.objects.filter(batctno=batchId).exists()
        return check


    def GetDays(self, value):
        days = value[1]
        if days == '1':
            return "Sunday"
        elif days == '2':
            return "Monday"
        elif days == '3':
            return "Tuesday"
        elif days == '4':
            return "Wednesday"
        elif days == '5':
            return "Thursday"
        elif days == '6':
            return "Friday"
        else:
            return "Saturday"
    

    def Add_Days(self, days, value):
        d = value[0]
        t = value[1]
        # print(d)
        if WorkingDays.objects.filter(day_code=t).exists():
            WorkingDays.objects.filter(day_code=t).update(status=d)
            pass
        else:
            create_day = WorkingDays(days=days, day_code=t)
            return create_day.save()
    

    def showWorkingDays(self, days):
        try:
            value = WorkingDays.objects.values('status').get(days=days)['status']
            return value
        except:
            value = ''
            return value
    

    def GetWorkingDays(self):
        now = datetime.datetime.now()
        # print(now.strftime("%A"))
        today = now.strftime("%A")
        try:
            w_status = WorkingDays.objects.values('status').get(days=today)['status']
            exist = WorkingDays.objects.filter(days=today).exists()
            if exist == True and w_status == '1':
                return True
            elif exist == False and w_status == '0':
                return False
            else:
                return False
        except:
            return False
    
    def CreateSinglePayment(self, batch_id, sname, saddress, s_db_acc, rname, raddress, r_cr_acc, rbank, amount, comment, status):
        single = SinglePayment(batch_id=batch_id, sname=sname, saddress=saddress, s_db_acc=s_db_acc, rname=rname, 
        raddress=raddress, r_cr_acc=r_cr_acc, rbank=rbank, amount=amount, comment=comment, status=status)
        return single.save()
    

    def showSingleTrans(self):
        show = ProcessBatch.objects.filter(Q(tnxtype='single') | Q(tnxtype='b2b')).order_by('-id')
        shows = SinglePayment.objects.filter()
        return show, shows
    
    def getUser(self):
        showUser = User.objects.filter().order_by('-id')
        return showUser

    def activateUser(self, id):
        User.objects.filter(id=id).update(is_active=True)
        pass

    def DeactivateUser(self, id):
        User.objects.filter(id=id).update(is_active=False)
        pass

    def UpdateUser(self, request, id, fullname, email):
        user_email = User.objects.values('email').get(id=id)['email']
        if User.objects.filter(email=email).exists() and user_email != email:
            messages.error(request, "Email Already used")
            return redirect(f'/updateuser/{id}')
        else:
            User.objects.filter(id=id).update(fullname=fullname, email=email)
            messages.success(request, "User Updated")
            return redirect(f'/updateuser/{id}')
    

    ### Delete User
    def DeleteUser(self, id):
        instance = User.objects.get(id=id)
        instance.delete()
        return redirect('/users')



    def CreateUser(self, request, fullname, email, password, role):
        if User.objects.filter(email=email).exists():
            messages.error(request, "User with this email already exist within the system")
            return redirect('/adduser')
        elif role == "":
            messages.error(request, "User Role Can't be empty")
            return redirect('/adduser')
        else:
            if role == "admin":
                cr_user = User.objects.create_user(fullname=fullname, email=email, password=password, is_superuser=True, is_staff=True)
                cr_user.save()
                messages.success(request, "User Created Successfully")
                return redirect('/adduser')
            
            else:
                cr_user = User.objects.create_user(fullname=fullname, email=email, password=password, is_staff=True)
                cr_user.save()
                messages.success(request, "User Created Successfully")
                return redirect('/adduser')
    

    def AddResponseMessage(self, request, msg, code):
        if ResponseMessage.objects.filter(code__startswith=code).exists():
            messages.error(request, "Response Code Exist")
            return redirect('/addmsg')
        else:
            cr_msg = ResponseMessage(code=code, message=msg)
            cr_msg.save()
            messages.success(request, "Response Message Added Successfully")
            return redirect('/addmsg')

    
    def ShowAllMessages(self):
        show = ResponseMessage.objects.filter()
        return show

    
    ### Get All Message for Update 
    def get_all_message(self, id):
        show = ResponseMessage.objects.all().get(id=id)
        return show

    
    #### Update Messages 
    def UpdateMessages(self, request, id, code, msg):
        if ResponseMessage.objects.filter(code=code).exists():
            messages.error(request, "Response Code Exist")
            return redirect(f'/updatemsg/{id}')
        else:
            ResponseMessage.objects.filter(id=id).update(code=code, message=msg)
            messages.success(request, "Response Message Updated")
            return redirect(f'/updatemsg/{id}')
    

    ### Delete Messages
    def DeleteMessage(self, id):
        instance = ResponseMessage.objects.get(id=id)
        instance.delete()
        return redirect('/allmsgs')

    
    def GetMsgCode(self, msgid):
        msgcode = MessageId.objects.values('msgname').get(msgid=msgid)['msgname']
        return msgcode

    def GetResponseMsg(self, code):
        msg = ResponseMessage.objects.values('message').get(code=code)['message']
        return msg