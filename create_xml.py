from typing import Counter
from xml.etree.ElementTree import XML
from django.contrib import messages
from django.http import request
from django.shortcuts import render, redirect
from function import *
from django.http import HttpResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.dispatch.dispatcher import receiver
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.core import serializers
from django.http import JsonResponse
Objects = Main()
import io
import csv
import ast
import uuid
import math
import datetime
import random
import string
import os
import openpyxl
import lxml.etree as ET
from lxml import etree
# import xml.etree.ElementTree as ET

script_dir = os.path.dirname(os.path.abspath(__file__))
dest_dir = os.path.join('static/out')
newpath = os.path.join('static/in')
atsPath = os.path.join('static/ats')


class CreateXMLS:

    def create_xml(self, request, batctno, amount, payee_id, currency, acctno, employee_name, bnkcode, gross_amount, tax_amount):
        identifier = request.user.identifier
        bicode = Participants.objects.values('biccode').get(identifier=identifier)['biccode']
        settl_acct = Participants.objects.values('settlement_acct').get(identifier=identifier)['settlement_acct']
        per_name = Participants.objects.values('par_name').get(identifier=identifier)['par_name']

        today = datetime.datetime.now()
        d = datetime.datetime.now().replace(tzinfo=datetime.timezone.utc)
        no_ms = d.replace(microsecond=0)
        now = no_ms.isoformat()
        tnow = today.strftime('%Y-%m-%d')

        current_time = today.strftime('%H:%M')

        U = 7
        ref = ''.join(random.choices(string.digits, k=U))
        txn = str(ref)
        txnref = f'FT{txn}'
        
        nsmap = {None: "urn:iso:std:iso:20022:tech:xsd:pacs.008.001.07", 'xsi':"http://www.w3.org/2001/XMLSchema-instance"}
            
            
        root = ET.Element('Document', nsmap=nsmap)
        m1 = ET.Element("FIToFICstmrCdtTrf")
        root.append(m1)

        GrpHdr = ET.Element("GrpHdr")
        m1.append(GrpHdr)

        MsgId = ET.SubElement(GrpHdr, "MsgId")
        MsgId.text = f'{batctno}'

        CreDtTm = ET.SubElement(GrpHdr, "CreDtTm")
        CreDtTm.text = f'{now}'

        NbOfTxs = ET.SubElement(GrpHdr, "NbOfTxs")
        NbOfTxs.text = f'1'

        TtlIntrBkSttlmAmt = ET.SubElement(GrpHdr, "TtlIntrBkSttlmAmt", Ccy="USD")
        TtlIntrBkSttlmAmt.text = f'{amount}'

        IntrBkSttlmDt = ET.SubElement(GrpHdr, "IntrBkSttlmDt")
        IntrBkSttlmDt.text = f'{tnow}'

        SttlmInf = ET.SubElement(GrpHdr, "SttlmInf")

        SttlmMtd = ET.SubElement(SttlmInf, "SttlmMtd")
        SttlmMtd.text = "CLRG"

        ClrSys = ET.SubElement(SttlmInf, "ClrSys")

        Prtry = ET.SubElement(ClrSys, "Prtry")
        Prtry.text = "ACH"

        InstgAgt = ET.SubElement(GrpHdr, "InstgAgt")

        FinInstnId = ET.SubElement(InstgAgt, "FinInstnId")

        BICFI = ET.SubElement(FinInstnId, "BICFI")
        BICFI.text = f'{bicode}'
        count = 000
                
        CdtTrfTxInf = ET.Element("CdtTrfTxInf")
        m1.append(CdtTrfTxInf)

        PmtId = ET.SubElement(CdtTrfTxInf, "PmtId")
        InstrId = ET.SubElement(PmtId, "InstrId")
        InstrId.text = f'001'

        EndToEndId = ET.SubElement(PmtId, "EndToEndId")
        EndToEndId.text = f'001'

        TxId = ET.SubElement(PmtId, "TxId")
        TxId.text = f'{payee_id}'


        PmtTpInf = ET.SubElement(CdtTrfTxInf, "PmtTpInf")
        SvcLvl = ET.SubElement(PmtTpInf, "SvcLvl")
        Cd = ET.SubElement(SvcLvl, "Cd")
        Cd.text = "SEPA"

        LclInstrm = ET.SubElement(PmtTpInf, "LclInstrm")
        Prtry = ET.SubElement(LclInstrm, "Prtry")
        Prtry.text = "B2B"

        CtgyPurp = ET.SubElement(PmtTpInf, "CtgyPurp")
        Cd = ET.SubElement(CtgyPurp, "Cd")
        Cd.text = "CASH"

        IntrBkSttlmAmt = ET.SubElement(CdtTrfTxInf, "IntrBkSttlmAmt", Ccy=f'{currency}')
        IntrBkSttlmAmt.text = f'{amount}'

        ChrgBr = ET.SubElement(CdtTrfTxInf, "ChrgBr")
        ChrgBr.text = "SLEV"

        Dbtr = ET.SubElement(CdtTrfTxInf, "Dbtr")
        Nm = ET.SubElement(Dbtr, "Nm")
        Nm.text = f'{per_name}'
        Id = ET.SubElement(Dbtr, "Id")
        OrgId = ET.SubElement(Id, "OrgId")
        AnyBIC = ET.SubElement(OrgId, "AnyBIC")
        AnyBIC.text = f'{bicode}'

        DbtrAcct = ET.SubElement(CdtTrfTxInf, "DbtrAcct")
        Id = ET.SubElement(DbtrAcct, "Id")
        Othr = ET.SubElement(Id, "Othr")
        Id = ET.SubElement(Othr, "Id")
        Id.text = f'{settl_acct}'

        DbtrAgt = ET.SubElement(CdtTrfTxInf, "DbtrAgt")
        FinInstnId = ET.SubElement(DbtrAgt, "FinInstnId")
        BICFI = ET.SubElement(FinInstnId, "BICFI")
        BICFI.text = f'{bicode}'

        CdtrAgt = ET.SubElement(CdtTrfTxInf, "CdtrAgt")
        FinInstnId = ET.SubElement(CdtrAgt, "FinInstnId")
        BICFI = ET.SubElement(FinInstnId, "BICFI")
        BICFI.text = f'{bnkcode}'

        Cdtr = ET.SubElement(CdtTrfTxInf, "Cdtr")
        Nm = ET.SubElement(Cdtr, "Nm")
        Nm.text = f'{employee_name}'
        Id = ET.SubElement(Cdtr, "Id")
        OrgId = ET.SubElement(Id, "OrgId")
        AnyBIC = ET.SubElement(OrgId, "AnyBIC")
        AnyBIC.text = f'{bnkcode}'

        CdtrAcct = ET.SubElement(CdtTrfTxInf, "CdtrAcct")
        Id = ET.SubElement(CdtrAcct, "Id")
        Othr = ET.SubElement(Id, "Othr")
        Id = ET.SubElement(Othr, "Id")
        Id.text = f'{acctno}'

        Purp = ET.SubElement(CdtTrfTxInf, "Purp")
        Cd = ET.SubElement(Purp, "Cd")
        Cd.text = "ADVA"

            
        def prettify(element, doctype=None, indent='  '):
            queue = [(0, element)]  # (level, element)
            while queue:
                level, element = queue.pop(0)
                children = [(level + 1, child) for child in list(element)]
                if children:
                    element.text = '\n' + indent * (level+1)  # for child open
                if queue:
                    element.tail = '\n' + indent * queue[0][0]  # for sibling open
                else:
                    element.tail = '\n' + indent * (level-1)  # for parent close
                queue[0:0] = children  # prepend so children come before siblings

        prettify(root)
        ET.ElementTree(root).write(f'{dest_dir}/{txnref}.xml', xml_declaration=False, encoding="utf-8")

        # file_log = File_Log(filename=filename, uploader=loggedin_user)
        # file_log.save()
        Payment.objects.filter(payee_id=payee_id).update(bnkcode=bnkcode, currency=currency, acctno=acctno, amount=amount, 
        employee_name=employee_name, gross_amount=gross_amount, tax_amount=tax_amount, status="modified")
        messages.success(request, f'The record with REF NO is {payee_id}')
        return redirect(f'/veiwTrans/{batctno}')

    
    def newXML(self, request, bcode, now, total_record, totalAmount, tnow, payload, rec_type, txnref, filename, loggedin_user, total_sum):
        nsmap = {None: "urn:iso:std:iso:20022:tech:xsd:pacs.008.001.07", 'xsi':"http://www.w3.org/2001/XMLSchema-instance"}
            
            
        root = ET.Element('Document', nsmap=nsmap)
        m1 = ET.Element("FIToFICstmrCdtTrf")
        root.append(m1)

        GrpHdr = ET.Element("GrpHdr")
        m1.append(GrpHdr)

        MsgId = ET.SubElement(GrpHdr, "MsgId")
        MsgId.text = f'{bcode}'

        CreDtTm = ET.SubElement(GrpHdr, "CreDtTm")
        CreDtTm.text = f'{now}'

        NbOfTxs = ET.SubElement(GrpHdr, "NbOfTxs")
        NbOfTxs.text = f'{total_record}'

        TtlIntrBkSttlmAmt = ET.SubElement(GrpHdr, "TtlIntrBkSttlmAmt", Ccy="USD")
        TtlIntrBkSttlmAmt.text = f'{totalAmount}'

        IntrBkSttlmDt = ET.SubElement(GrpHdr, "IntrBkSttlmDt")
        IntrBkSttlmDt.text = f'{tnow}'

        SttlmInf = ET.SubElement(GrpHdr, "SttlmInf")

        SttlmMtd = ET.SubElement(SttlmInf, "SttlmMtd")
        SttlmMtd.text = "CLRG"

        ClrSys = ET.SubElement(SttlmInf, "ClrSys")

        Prtry = ET.SubElement(ClrSys, "Prtry")
        Prtry.text = "ACH"

        InstgAgt = ET.SubElement(GrpHdr, "InstgAgt")

        FinInstnId = ET.SubElement(InstgAgt, "FinInstnId")

        BICFI = ET.SubElement(FinInstnId, "BICFI")
        BICFI.text = "CBSOSOSM"
        count = 000
        for rec in payload:
            L = 10
            pid = ''.join(random.choices(string.digits, k=L))
            pay_id = str(pid)
            count += 1
            cid = (f'00{count}')
            # pay_id = rec['pay_id']
            emp_name = rec['emp_name']
            ben_bicode = rec['ben_bicode']
            ben_bank_name = rec['ben_bank_name']
            acct_no = rec['acct_no']
            currency = rec['currency']
            g_amt = rec['g_amt']
            tax_amt = rec['tax_amt']
            amount = rec['amount']
            desc = rec['desc']
            CdtTrfTxInf = ET.Element("CdtTrfTxInf")
            m1.append(CdtTrfTxInf)

            PmtId = ET.SubElement(CdtTrfTxInf, "PmtId")
            InstrId = ET.SubElement(PmtId, "InstrId")
            InstrId.text = f'{cid}'

            EndToEndId = ET.SubElement(PmtId, "EndToEndId")
            EndToEndId.text = f'{cid}'

            TxId = ET.SubElement(PmtId, "TxId")
            TxId.text = f'{pay_id}'


            PmtTpInf = ET.SubElement(CdtTrfTxInf, "PmtTpInf")
            SvcLvl = ET.SubElement(PmtTpInf, "SvcLvl")
            Cd = ET.SubElement(SvcLvl, "Cd")
            Cd.text = "SEPA"

            LclInstrm = ET.SubElement(PmtTpInf, "LclInstrm")
            Prtry = ET.SubElement(LclInstrm, "Prtry")
            Prtry.text = "B2B"

            CtgyPurp = ET.SubElement(PmtTpInf, "CtgyPurp")
            Cd = ET.SubElement(CtgyPurp, "Cd")
            Cd.text = "CASH"

            IntrBkSttlmAmt = ET.SubElement(CdtTrfTxInf, "IntrBkSttlmAmt", Ccy=f'{currency}')
            IntrBkSttlmAmt.text = f'{amount}'

            ChrgBr = ET.SubElement(CdtTrfTxInf, "ChrgBr")
            ChrgBr.text = "SLEV"

            Dbtr = ET.SubElement(CdtTrfTxInf, "Dbtr")
            Nm = ET.SubElement(Dbtr, "Nm")
            Nm.text = f'{ben_bank_name}'
            Id = ET.SubElement(Dbtr, "Id")
            OrgId = ET.SubElement(Id, "OrgId")
            AnyBIC = ET.SubElement(OrgId, "AnyBIC")
            AnyBIC.text = f'{ben_bicode}'

            DbtrAcct = ET.SubElement(CdtTrfTxInf, "DbtrAcct")
            Id = ET.SubElement(DbtrAcct, "Id")
            Othr = ET.SubElement(Id, "Othr")
            Id = ET.SubElement(Othr, "Id")
            Id.text = f'{acct_no}'

            DbtrAgt = ET.SubElement(CdtTrfTxInf, "DbtrAgt")
            FinInstnId = ET.SubElement(DbtrAgt, "FinInstnId")
            BICFI = ET.SubElement(FinInstnId, "BICFI")
            BICFI.text = f'{ben_bicode}'

            CdtrAgt = ET.SubElement(CdtTrfTxInf, "CdtrAgt")
            FinInstnId = ET.SubElement(CdtrAgt, "FinInstnId")
            BICFI = ET.SubElement(FinInstnId, "BICFI")
            BICFI.text = f'{ben_bicode}'

            Cdtr = ET.SubElement(CdtTrfTxInf, "Cdtr")
            Nm = ET.SubElement(Cdtr, "Nm")
            Nm.text = f'{emp_name}'
            Id = ET.SubElement(Cdtr, "Id")
            OrgId = ET.SubElement(Id, "OrgId")
            AnyBIC = ET.SubElement(OrgId, "AnyBIC")
            AnyBIC.text = f'{ben_bicode}'

            CdtrAcct = ET.SubElement(CdtTrfTxInf, "CdtrAcct")
            Id = ET.SubElement(CdtrAcct, "Id")
            Othr = ET.SubElement(Id, "Othr")
            Id = ET.SubElement(Othr, "Id")
            Id.text = f'{acct_no}'

            Purp = ET.SubElement(CdtTrfTxInf, "Purp")
            Cd = ET.SubElement(Purp, "Cd")
            Cd.text = "ADVA"
            if Objects.check_availability(ben_bicode) == False:
                messages.error(request, f'{ben_bicode} is not a valid biccode or does not exist')
                return redirect('/upload')
            
            Objects.pay(request, rec_type, pay_id, emp_name, ben_bicode, ben_bank_name, acct_no, currency, g_amt, tax_amt, amount, bcode, desc, status='created')
            
        def prettify(element, doctype=None, indent='  '):
            queue = [(0, element)]  # (level, element)
            while queue:
                level, element = queue.pop(0)
                children = [(level + 1, child) for child in list(element)]
                if children:
                    element.text = '\n' + indent * (level+1)  # for child open
                if queue:
                    element.tail = '\n' + indent * queue[0][0]  # for sibling open
                else:
                    element.tail = '\n' + indent * (level-1)  # for parent close
                queue[0:0] = children  # prepend so children come before siblings

        prettify(root)
        ET.ElementTree(root).write(f'{dest_dir}/{txnref}.xml', xml_declaration=False, encoding="utf-8")

        file_log = File_Log(filename=filename, uploader=loggedin_user)
        file_log.save()
        Objects.Create_File_Management(bcode, txnref)
        messages.success(request, f'You Pay Load has been processed and your BATCH NO is {bcode}')
        pbatch = ProcessBatch(batch_id=bcode, tnxtype=rec_type, total_amt=totalAmount, num_of_trans=total_record, author=loggedin_user, date=now)
        return pbatch.save()

    

    def ReDOnewXML(self, bcode, now, total_record, totalAmount, tnow, payload, txnref):
        nsmap = {None: "urn:iso:std:iso:20022:tech:xsd:pacs.008.001.07", 'xsi':"http://www.w3.org/2001/XMLSchema-instance"}
            
            
        root = ET.Element('Document', nsmap=nsmap)
        m1 = ET.Element("FIToFICstmrCdtTrf")
        root.append(m1)

        GrpHdr = ET.Element("GrpHdr")
        m1.append(GrpHdr)

        MsgId = ET.SubElement(GrpHdr, "MsgId")
        MsgId.text = f'{bcode}'

        CreDtTm = ET.SubElement(GrpHdr, "CreDtTm")
        CreDtTm.text = f'{now}'

        NbOfTxs = ET.SubElement(GrpHdr, "NbOfTxs")
        NbOfTxs.text = f'{total_record}'

        TtlIntrBkSttlmAmt = ET.SubElement(GrpHdr, "TtlIntrBkSttlmAmt", Ccy="USD")
        TtlIntrBkSttlmAmt.text = f'{totalAmount}'

        IntrBkSttlmDt = ET.SubElement(GrpHdr, "IntrBkSttlmDt")
        IntrBkSttlmDt.text = f'{tnow}'

        SttlmInf = ET.SubElement(GrpHdr, "SttlmInf")

        SttlmMtd = ET.SubElement(SttlmInf, "SttlmMtd")
        SttlmMtd.text = "CLRG"

        ClrSys = ET.SubElement(SttlmInf, "ClrSys")

        Prtry = ET.SubElement(ClrSys, "Prtry")
        Prtry.text = "ACH"

        InstgAgt = ET.SubElement(GrpHdr, "InstgAgt")

        FinInstnId = ET.SubElement(InstgAgt, "FinInstnId")

        BICFI = ET.SubElement(FinInstnId, "BICFI")
        BICFI.text = "CBSOSOSM"
        count = 000
        for rec in payload:
            L = 10
            pid = ''.join(random.choices(string.digits, k=L))
            pay_id = str(pid)
            count += 1
            cid = (f'00{count}')
            # pay_id = rec['pay_id']
            emp_name = rec['emp_name']
            ben_bicode = rec['ben_bicode']
            ben_bank_name = rec['ben_bank_name']
            acct_no = rec['acct_no']
            currency = rec['currency']
            g_amt = rec['g_amt']
            tax_amt = rec['tax_amt']
            amount = rec['amount']
            desc = rec['desc']
            CdtTrfTxInf = ET.Element("CdtTrfTxInf")
            m1.append(CdtTrfTxInf)

            PmtId = ET.SubElement(CdtTrfTxInf, "PmtId")
            InstrId = ET.SubElement(PmtId, "InstrId")
            InstrId.text = f'{cid}'

            EndToEndId = ET.SubElement(PmtId, "EndToEndId")
            EndToEndId.text = f'{cid}'

            TxId = ET.SubElement(PmtId, "TxId")
            TxId.text = f'{pay_id}'


            PmtTpInf = ET.SubElement(CdtTrfTxInf, "PmtTpInf")
            SvcLvl = ET.SubElement(PmtTpInf, "SvcLvl")
            Cd = ET.SubElement(SvcLvl, "Cd")
            Cd.text = "SEPA"

            LclInstrm = ET.SubElement(PmtTpInf, "LclInstrm")
            Prtry = ET.SubElement(LclInstrm, "Prtry")
            Prtry.text = "B2B"

            CtgyPurp = ET.SubElement(PmtTpInf, "CtgyPurp")
            Cd = ET.SubElement(CtgyPurp, "Cd")
            Cd.text = "CASH"

            IntrBkSttlmAmt = ET.SubElement(CdtTrfTxInf, "IntrBkSttlmAmt", Ccy=f'{currency}')
            IntrBkSttlmAmt.text = f'{amount}'

            ChrgBr = ET.SubElement(CdtTrfTxInf, "ChrgBr")
            ChrgBr.text = "SLEV"

            Dbtr = ET.SubElement(CdtTrfTxInf, "Dbtr")
            Nm = ET.SubElement(Dbtr, "Nm")
            Nm.text = f'{ben_bank_name}'
            Id = ET.SubElement(Dbtr, "Id")
            OrgId = ET.SubElement(Id, "OrgId")
            AnyBIC = ET.SubElement(OrgId, "AnyBIC")
            AnyBIC.text = f'{ben_bicode}'

            DbtrAcct = ET.SubElement(CdtTrfTxInf, "DbtrAcct")
            Id = ET.SubElement(DbtrAcct, "Id")
            Othr = ET.SubElement(Id, "Othr")
            Id = ET.SubElement(Othr, "Id")
            Id.text = f'{acct_no}'

            DbtrAgt = ET.SubElement(CdtTrfTxInf, "DbtrAgt")
            FinInstnId = ET.SubElement(DbtrAgt, "FinInstnId")
            BICFI = ET.SubElement(FinInstnId, "BICFI")
            BICFI.text = f'{ben_bicode}'

            CdtrAgt = ET.SubElement(CdtTrfTxInf, "CdtrAgt")
            FinInstnId = ET.SubElement(CdtrAgt, "FinInstnId")
            BICFI = ET.SubElement(FinInstnId, "BICFI")
            BICFI.text = f'{ben_bicode}'

            Cdtr = ET.SubElement(CdtTrfTxInf, "Cdtr")
            Nm = ET.SubElement(Cdtr, "Nm")
            Nm.text = f'{emp_name}'
            Id = ET.SubElement(Cdtr, "Id")
            OrgId = ET.SubElement(Id, "OrgId")
            AnyBIC = ET.SubElement(OrgId, "AnyBIC")
            AnyBIC.text = f'{ben_bicode}'

            CdtrAcct = ET.SubElement(CdtTrfTxInf, "CdtrAcct")
            Id = ET.SubElement(CdtrAcct, "Id")
            Othr = ET.SubElement(Id, "Othr")
            Id = ET.SubElement(Othr, "Id")
            Id.text = f'{acct_no}'

            Purp = ET.SubElement(CdtTrfTxInf, "Purp")
            Cd = ET.SubElement(Purp, "Cd")
            Cd.text = "ADVA"

            ProcessBatch.objects.filter(batch_id=bcode).update(status='created')
            Payment.objects.filter(batctno=bcode).update(status='created')
            
        def prettify(element, doctype=None, indent='  '):
            queue = [(0, element)]  # (level, element)
            while queue:
                level, element = queue.pop(0)
                children = [(level + 1, child) for child in list(element)]
                if children:
                    element.text = '\n' + indent * (level+1)  # for child open
                if queue:
                    element.tail = '\n' + indent * queue[0][0]  # for sibling open
                else:
                    element.tail = '\n' + indent * (level-1)  # for parent close
                queue[0:0] = children  # prepend so children come before siblings

        prettify(root)
        ET.ElementTree(root).write(f'{dest_dir}/{txnref}.xml', xml_declaration=False, encoding="utf-8")
        Objects.Create_File_Management(bcode, txnref)
        pass


    def SingleXML(self, amount, comment, dname, dAddress, dAccount, crdBiccode, cname, cAddress, cAccount, bcode, txnref):
    
        today = datetime.datetime.now()
        d = datetime.datetime.now().replace(tzinfo=datetime.timezone.utc)
        no_ms = d.replace(microsecond=0)
        now = no_ms.isoformat()
        tnow = today.strftime('%Y-%m-%d')

        biccode = Objects.Configuration(name='biccode')

        nsmap = {None: "urn:swift:saa:xsd:saa.2.0"}
        attr_qname = etree.QName("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation")
        ns = {None: "urn:iso:std:iso:20022:tech:xsd:head.001.001.02", 'xsi':"http://www.w3.org/2001/XMLSchema-instance"}
        k = {None: "http://www.w3.org/2000/09/xmldsig#"}
        root = ET.Element('DataPDU', nsmap=nsmap)

        rev = ET.SubElement(root, "Revision")
        rev.text = "2.0.7"

        header = ET.SubElement(root, "Header")
        message = ET.SubElement(header, "Message")
        sendRef = ET.SubElement(message, "SenderReference")
        sendRef.text = f'{txnref}'
        msgId = ET.SubElement(message, "MessageIdentifier")
        msgId.text = "pacs.008.001.09"
        sformat = ET.SubElement(message, "Format")
        sformat.text = "AnyXML"
        sender = ET.SubElement(message, "Sender")
        BIC12 = ET.SubElement(sender, "BIC12")
        BIC12.text = f'{biccode}'
        fullname = ET.SubElement(sender, "FullName")
        X1 = ET.SubElement(fullname, "X1")
        X1.text = f'{biccode}'
        receiver = ET.SubElement(message, "Receiver")
        BIC12 = ET.SubElement(receiver, "BIC12")
        BIC12.text = f'{crdBiccode}'
        rfullname = ET.SubElement(receiver, "FullName")
        X1 = ET.SubElement(rfullname, "X1")
        X1.text = f'{crdBiccode}'
        int_face_info = ET.SubElement(message, "InterfaceInfo")
        user_ref = ET.SubElement(int_face_info, "UserReference")
        user_ref.text = f'{txnref}'
        net_info = ET.SubElement(message, "NetworkInfo")
        priority = ET.SubElement(net_info, "Priority")
        priority.text = "Normal"
        net = ET.SubElement(net_info, "Network")
        net.text = "Application"

        body = ET.SubElement(root, "Body")
        apphrd = ET.SubElement(body, 'AppHdr', {attr_qname: 'urn:iso:std:iso:20022:tech:xsd:head.001.001.02 head.001.001.02.xsd'}, nsmap=ns)
        fr = ET.SubElement(apphrd, "Fr")
        FIId = ET.SubElement(fr, "FIId")
        FinInstnId = ET.SubElement(FIId, "FinInstnId")
        BICFI = ET.SubElement(FinInstnId, "BICFI")
        BICFI.text = f'{biccode}'
        to = ET.SubElement(apphrd, "To")
        FIId = ET.SubElement(to, "FIId")
        FinInstnId = ET.SubElement(FIId, "FinInstnId")
        BICFI = ET.SubElement(FinInstnId, "BICFI")
        BICFI.text = f'{crdBiccode}'
        BizMsgIdr = ET.SubElement(apphrd, "BizMsgIdr")
        BizMsgIdr.text = f'{txnref}'
        MsgDefIdr = ET.SubElement(apphrd, "MsgDefIdr")
        MsgDefIdr.text = "pacs.008.001.09"
        BizSvc = ET.SubElement(apphrd, "BizSvc")
        BizSvc.text = "RTGSFIToFICustomerCredit"
        CreDt = ET.SubElement(apphrd, "CreDt")
        CreDt.text = f'{now}'
        Sgntr = ET.SubElement(apphrd, "Sgntr")
        XMLSgntrs = ET.SubElement(Sgntr, "XMLSgntrs", nsmap=k)
        XMLSgntrs.text = "No signature"

        doc = ET.SubElement(body, "Document", {attr_qname: 'urn:iso:std:iso:20022:tech:xsd:head.001.001.02 head.001.001.02.xsd'}, nsmap=ns)
        FIToFICstmrCdtTrf = ET.SubElement(doc, "FIToFICstmrCdtTrf")
        GrpHdr = ET.SubElement(FIToFICstmrCdtTrf, "GrpHdr")
        MsgId = ET.SubElement(GrpHdr, "MsgId")
        MsgId.text = f'{bcode}'
        CreDtTm = ET.SubElement(GrpHdr, "CreDtTm")
        CreDtTm.text = f'{now}'
        NbOfTxs = ET.SubElement(GrpHdr, "NbOfTxs")
        NbOfTxs.text = "1"
        TtlIntrBkSttlmAmt = ET.SubElement(GrpHdr, "TtlIntrBkSttlmAmt", Ccy="USD")
        TtlIntrBkSttlmAmt.text = f'{amount}'
        IntrBkSttlmDt = ET.SubElement(GrpHdr, "IntrBkSttlmDt")
        IntrBkSttlmDt.text = f'{tnow}'
        SttlmInf = ET.SubElement(GrpHdr, "SttlmInf")
        SttlmMtd = ET.SubElement(SttlmInf, "SttlmMtd")
        SttlmMtd.text = "CLRG"
        InstgAgt = ET.SubElement(GrpHdr, "InstgAgt")
        FinInstnId = ET.SubElement(InstgAgt, "FinInstnId")
        BICFI = ET.SubElement(FinInstnId, "BICFI")
        BICFI.text = f'{biccode}'

        ### Another One
        InstgAgt = ET.SubElement(GrpHdr, "InstgAgt")
        FinInstnId = ET.SubElement(InstgAgt, "FinInstnId")
        BICFI = ET.SubElement(FinInstnId, "BICFI")
        BICFI.text = f'{biccode}'

        CdtTrfTxInf = ET.SubElement(FIToFICstmrCdtTrf, "CdtTrfTxInf")
        PmtId = ET.SubElement(CdtTrfTxInf, "PmtId")
        EndToEndId = ET.SubElement(PmtId, "EndToEndId")
        EndToEndId.text = f'{txnref}'
        TxId = ET.SubElement(PmtId, "TxId")
        TxId.text = f'{txnref}'
        PmtTpInf = ET.SubElement(CdtTrfTxInf, "PmtTpInf")
        InstrPrty = ET.SubElement(PmtTpInf, "InstrPrty")
        InstrPrty.text = "HIGH"
        LclInstrm = ET.SubElement(PmtTpInf, "LclInstrm")
        Prtry = ET.SubElement(LclInstrm, "Prtry")
        Prtry.text = "RTGSFIToFICustomerCredit"
        CtgyPurp = ET.SubElement(PmtTpInf, "CtgyPurp")
        Cd = ET.SubElement(CtgyPurp, "Cd")
        Cd.text = "CASH"
        IntrBkSttlmAmt = ET.SubElement(CdtTrfTxInf, "IntrBkSttlmAmt", Ccy="USD")
        IntrBkSttlmAmt.text = f'{amount}'
        ChrgBr = ET.SubElement(CdtTrfTxInf, "ChrgBr")
        ChrgBr.text = "DEBT"
        Dbtr = ET.SubElement(CdtTrfTxInf, "Dbtr")
        Nm = ET.SubElement(Dbtr, "Nm")
        Nm.text = f'{dname}'
        PstlAdr = ET.SubElement(Dbtr, "PstlAdr")
        AdrLine = ET.SubElement(PstlAdr, "AdrLine")
        AdrLine.text = f'{dAddress}'
        DbtrAcct = ET.SubElement(CdtTrfTxInf, "DbtrAcct")
        Id = ET.SubElement(DbtrAcct, "Id")
        Othr = ET.SubElement(Id, "Othr")
        Id = ET.SubElement(Othr, "Id")
        Id.text = f'{dAccount}'
        DbtrAgt = ET.SubElement(CdtTrfTxInf, "DbtrAgt")
        FinInstnId = ET.SubElement(DbtrAgt, "FinInstnId")
        BICFI = ET.SubElement(FinInstnId, "BICFI")
        BICFI.text = f'{biccode}'
        CdtrAgt = ET.SubElement(CdtTrfTxInf, "CdtrAgt")
        FinInstnId = ET.SubElement(CdtrAgt, "FinInstnId")
        BICFI = ET.SubElement(FinInstnId, "BICFI")
        BICFI.text = f'{crdBiccode}'
        Cdtr = ET.SubElement(CdtTrfTxInf, "Cdtr")
        Nm = ET.SubElement(Cdtr, "Nm")
        Nm.text = f'{cname}'
        PstlAdr = ET.SubElement(Cdtr, "PstlAdr")
        AdrLine = ET.SubElement(PstlAdr, "AdrLine")
        AdrLine.text = f'{cAddress}'

        CdtrAcct = ET.SubElement(CdtTrfTxInf, "CdtrAcct")
        Id = ET.SubElement(CdtrAcct, "Id")
        Othr = ET.SubElement(Id, "Othr")
        Id = ET.SubElement(Othr, "Id")
        Id.text = f'{cAccount}'

        RmtInf = ET.SubElement(CdtTrfTxInf, "RmtInf")
        Ustrd = ET.SubElement(RmtInf, "Ustrd")
        Ustrd.text = f'{comment}'

        def prettify(element, indent='  '):
            queue = [(0, element)]  # (level, element)
            while queue:
                level, element = queue.pop(0)
                children = [(level + 1, child) for child in list(element)]
                if children:
                    element.text = '\n' + indent * (level+1)  # for child open
                if queue:
                    element.tail = '\n' + indent * queue[0][0]  # for sibling open
                else:
                    element.tail = '\n' + indent * (level-1)  # for parent close
                queue[0:0] = children  # prepend so children come before siblings

        prettify(root)
        ET.ElementTree(root).write(f'{atsPath}/{txnref}.xml', xml_declaration=True, encoding="utf-8")